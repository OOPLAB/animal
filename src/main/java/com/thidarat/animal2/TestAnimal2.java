/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.animal2;

/**
 *
 * @author User
 */
public class TestAnimal2 {

    public static void main(String[] args) {
        Human h1 = new Human("Dang", 2);
        h1.eat();
        h1.walk();
        h1.run();
        h1.sleep();
        System.out.println("h1 is Animal ? " + (h1 instanceof Animal)); // instanceof = การเช็คว่าเป็น Animal ไหม
        System.out.println("h1 is LandAnimal ? " + (h1 instanceof LandAnimal)); // instanceof = การเช็คว่าเป็น LandAnimal ไหม
        System.out.println("---------------------------------------");
        Animal a1 = h1;
        System.out.println("a1 is Animal ? " + (a1 instanceof LandAnimal)); // instanceof = การเช็คว่าเป็น LandAnimal ไหม
        System.out.println("a1 is Reptile ? " + (a1 instanceof Reptile)); // instanceof = การเช็คว่าเป็น Reptile ไหม
        System.out.println("---------------------------------------");

        Cat c1 = new Cat("Dew", 4);
        c1.eat();
        c1.run();
        c1.speak();

        System.out.println("c1 is Animal ? " + (c1 instanceof Animal)); // instanceof = การเช็คว่าเป็น Animal ไหม
        System.out.println("c1 is LandAnimal ? " + (c1 instanceof LandAnimal)); // instanceof = การเช็คว่าเป็น LandAnimal ไหม
        System.out.println("---------------------------------------");

        Dog d1 = new Dog("Boom", 4);
        d1.eat();
        d1.run();
        d1.speak();
        d1.walk();

        System.out.println("d1 is Animal ? " + (d1 instanceof Animal)); // instanceof = การเช็คว่าเป็น Animal ไหม
        System.out.println("d1 is LandAnimal ? " + (d1 instanceof LandAnimal)); // instanceof = การเช็คว่าเป็น LandAnimal ไหม
        System.out.println("---------------------------------------");

        Crocodile cro1 = new Crocodile("Far", 0);
        cro1.eat();
        cro1.crawl();
        cro1.speak();
        cro1.walk();

        System.out.println("cro1 is Animal ? " + (cro1 instanceof Animal)); // instanceof = การเช็คว่าเป็น Animal ไหม
        System.out.println("cro1 is Reptile ? " + (cro1 instanceof Reptile)); // instanceof = การเช็คว่าเป็น Reptile ไหม
        System.out.println("---------------------------------------");

        Snakes s1 = new Snakes("It", 0);
        s1.eat();
        s1.crawl();
        s1.speak();
        s1.walk();

        System.out.println("s1 is Animal ? " + (s1 instanceof Animal)); // instanceof = การเช็คว่าเป็น Animal ไหม
        System.out.println("s1 is Reptile ? " + (s1 instanceof Reptile)); // instanceof = การเช็คว่าเป็น Reptile ไหม
        System.out.println("---------------------------------------");

        Bat b1 = new Bat("Green", 2);
        b1.eat();
        b1.sleep();

        System.out.println("b1 is Animal ? " + (b1 instanceof Animal)); // instanceof = การเช็คว่าเป็น Animal ไหม
        System.out.println("b1 is Poultry ? " + (b1 instanceof Poultry)); // instanceof = การเช็คว่าเป็นPoultry ไหม
        System.out.println("---------------------------------------");

        Bird bir1 = new Bird("Red", 2);
        bir1.eat();
        bir1.sleep();

        System.out.println("bir1 is Animal ? " + (bir1 instanceof Animal)); // instanceof = การเช็คว่าเป็น Animal ไหม
        System.out.println("bir1 is Poultry ? " + (bir1 instanceof Poultry)); // instanceof = การเช็คว่าเป็นPoultry ไหม
        System.out.println("---------------------------------------");

        Fish f1 = new Fish("Yellow");
        f1.eat();
        f1.swim();
        f1.sleep();

        System.out.println("f1 is Animal ? " + (f1 instanceof Animal)); // instanceof = การเช็คว่าเป็น Animal ไหม
        System.out.println("f1 is AquaticAnimal  ? " + (f1 instanceof AquaticAnimal)); // instanceof = การเช็คว่าเป็น AquaticAnimal  ไหม
        System.out.println("---------------------------------------");

        Crab crab1 = new Crab("Blue");
        crab1.eat();
        crab1.swim();
        crab1.sleep();

        System.out.println("crab1 is Animal ? " + (crab1 instanceof Animal)); // instanceof = การเช็คว่าเป็น Animal ไหม
        System.out.println("crab1 is AquaticAnimal  ? " + (crab1 instanceof AquaticAnimal)); // instanceof = การเช็คว่าเป็น AquaticAnimal  ไหม
        System.out.println("---------------------------------------");
        Plane plane = new Plane("Engine numeber I ");

        b1.fly(); //Animal Poultry Flyable 
        plane.fly(); //Vahide Flyable
        System.out.println("---------------------------------------");
        
        
        
        Flyable[] flyable = {b1, plane}; //จัดกลุ่มที่บินได้
        for (Flyable f : flyable) {
            if(f instanceof Plane){
                Plane p= (Plane)f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
        
         System.out.println("---------------------------------------");
         
        Runable[] runable ={d1,plane};
        for (Runable r: runable){
            r.run();
            
        }
    }
}
