/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.animal2;

/**
 *
 * @author User
 */
public class Crocodile extends Reptile{

    private String nickname;
    public Crocodile(String name, int numberOfLegs) {
        super(name, numberOfLegs);
        this.nickname=name;
    }

    @Override
    public void crawl() {
       System.out.println("Crocodile : " + nickname + "  crawl");
    }

    @Override
    public void eat() {
        System.out.println("Crocodile : " + nickname + "  eat");
    }

    @Override
    public void walk() {
        System.out.println("Crocodile : " + nickname + "  walk");
    }

    @Override
    public void speak() {
        System.out.println("Crocodile : " + nickname + "  speak : ");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodile : " + nickname + "  slepp");
    }
    
}
