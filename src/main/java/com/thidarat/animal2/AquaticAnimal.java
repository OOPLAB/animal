/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.animal2;

/**
 *
 * @author User
 */
public  abstract class AquaticAnimal extends Animal { // abstract class  and extends Animal 

    public AquaticAnimal(String name) {
        super(name,0);
    }
    
    public abstract void swim(); //abstract method
    
}
