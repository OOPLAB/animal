/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.animal2;

/**
 *
 * @author User
 */
public class Fish extends AquaticAnimal {
 private String nickname;

    public Fish(String name) {
        super(name);
        this.nickname=name;
    }
 
    @Override
    public void swim() {
        System.out.println("Fish : " + nickname + "  swim");
    }

    @Override
    public void eat() {
        System.out.println("Fish : " + nickname + "  eat");
    }

    @Override
    public void sleep() {
        System.out.println("Fish : " + nickname + "  slepp");
    }

    @Override
    public void walk() {
        System.out.println("Fish : " + nickname + "  Nowalk");
    }

    @Override
    public void speak() {
         System.out.println("Fish : " + nickname + "  Nospeak");
    }
}
