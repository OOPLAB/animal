/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.animal2;

/**
 *
 * @author User
 */
public class Snakes extends Reptile {
    private String nickname;
    
    public Snakes(String name, int numberOfLegs) {
        super(name, numberOfLegs);
        this.nickname=name;
    }

    @Override
    public void crawl() {
       System.out.println("Snakes : " + nickname + "  crawl");
    }

    @Override
    public void eat() {
        System.out.println("Snakes : " + nickname + "  eat");
    }

    @Override
    public void walk() {
        System.out.println("Snakes : " + nickname + "  walk");
    }

    @Override
    public void speak() {
        System.out.println("Snakes : " + nickname + "  speak : Hiss Hiss");
    }

    @Override
    public void sleep() {
        System.out.println("Snakes : " + nickname + "  slepp");
    }
    
}
