/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.animal2;

/**
 *
 * @author User
 */
public class Bat extends Poultry {
 private String nickname;

    public Bat(String name, int numberOfLegs) {
        super();
    }
 
  @Override
    public void eat() {
        System.out.println("Bat : " + nickname + "  eat");
    }

    @Override
    public void walk() {
        System.out.println("Bat : " + nickname + "  walk");
    }

    @Override
    public void speak() {
        System.out.println("Bat : " + nickname + "  speak : Screech Screeh");
    }

    @Override
    public void sleep() {
        System.out.println("Bat : " + nickname + "  slepp");
    }

    @Override
    public void fly() {
     System.out.println("Bat : fly");
    }
    
    
}
