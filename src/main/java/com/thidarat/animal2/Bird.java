/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.animal2;

/**
 *
 * @author User
 */
public class Bird extends Poultry{
    private String nickname;

    public Bird(String name, int numberOfLegs) {
        super();
        this.nickname=name;
    }
 
  @Override
    public void eat() {
        System.out.println("Bird : " + nickname + "  eat");
    }

    @Override
    public void walk() {
        System.out.println("Bird : " + nickname + "  walk");
    }

    @Override
    public void speak() {
        System.out.println("Bird : " + nickname + "  speak : Tweet  Tweet");
    }

    @Override
    public void sleep() {
        System.out.println("Bird : " + nickname + "  slepp");
    }

    @Override
    public void fly() {
         System.out.println("Bird : " + nickname + "  fly"); 
    }
}
