/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.animal2;

/**
 *
 * @author User
 */
public abstract class Reptile extends Animal { // abstract class  and extends Animal 

    public Reptile(String name, int numberOfLegs) {
        super(name, numberOfLegs);
    }
    
    public abstract void crawl(); //abstract method
}
