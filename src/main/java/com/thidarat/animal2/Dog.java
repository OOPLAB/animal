/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.animal2;

/**
 *
 * @author User
 */
public class Dog  extends LandAnimal{
     private String nickname;

    public Dog(String name, int numberOfLegs) {
        super(name, numberOfLegs);
         this.nickname=name;
    }

    @Override
    public void run() {
        System.out.println("Dog : " + nickname + "  run");
    }

    @Override
    public void eat() {
        System.out.println("Dog : " + nickname + "  eat");
    }

    @Override
    public void walk() {
        System.out.println("Dog : " + nickname + "  walk");
    }

    @Override
    public void speak() {
        System.out.println("Dog : " + nickname + "  speak : Box Box");
    }

    @Override
    public void sleep() {
        System.out.println("Dog : " + nickname + "  sleep");
    }

}
